<p>The <strong>Advanced Help</strong> framework was designed to replace the original Drupal help system, which has several flaws that make it hard to create new help, hard to maintain existing help, and particularly hard to access help.</p>

<p>The primary goal, then, is to increase the accessibility of help, meaning the ability of both the user and the help text author to access the needed tools to use, create, maintain and translate the help.</p>

<p>This system is completely separate from Drupal's <code>hook_help()</code>.</p>

<p>Messages added to the top of a page are not really “help”. Often these messages are an introduction to a form or a short blurb telling a user what to do with a particular page. The problem is, these messages are always there, they are easily ignored, and they come before the actual page. In general, when users are learning, they want to see the page first, then ask questions. The reverse order is much less conducive to actually teaching a user how to use something. By allowing help to be available on request, the system conforms more naturally to how most people work.</p>

<h2>Advanced Help is organized by topic</h2>

<p>With the <code>hook_help()</code> method, help text is organized by URL path. This is fine if you have help text describing how to use a particular page or what a particular page does, but ultimately is limiting because manuals and documentation are usually grouped by topic, and those topics are determined by the material itself.</p>

<p><strong>Advanced Help</strong> allows the documentation author to organize topics as he or she sees fit; the only restriction, really, is that each individual chunk of text needs to stand on its own as a discrete topic.</p>

<p>What's more, modules and themes can insert their topics into another's hierarchy. This would allow the Drupal core to create a task based help navigation system which allows modules and themes to insert topics into that navigation fluidly. This allows modules and themes to continue to keep their systems separate, yet integrate into the main system.</p>

<h2>Advanced Help topics are in their own files </h2>

<p>This separation makes it easy to find and modify. When everything is lumped together in <code>hook_help()</code> in PHP strings that are run through <code>t()</code>, here is a fair amount of PHP code necessary in this system that actually gets in the way of writing good, explanatory text.</p>

<p>In fact, requiring a documentation author to embed documentation in PHP at all is a detriment. Advanced Help let the documentation author use html (Hyper Text Markup Language) or md (MarkDown) which are more accessible.</p>

<p>Another benefit to not using PHP is that the files themselves are safe. They are filtered to escape malicious script code.</p>

<h2>Advanced Help files are translated as a file</h2>

<p>It is actually not easy to translate documents as strings, particularly when the language being used is very much unlike English. In fact, when translating a document, the organization of the sentences may change drastically. It is also a burden on the CPU to do this, as you are indexing on very long strings.</p>

<p>Translators have a much better time translating a document as a unit, because of the presence of the entire context.</p>

<h2>Advanced Help has its own navigation system</h2>

<p>By making use of a navigation system specified in a <code>.help.yml</code> file (which is not PHP code and therefore safe to use), the help can be structured like a book, which is typical of online manuals. This is familiar to users, can be organized (and re-organized) and allows a module or theme to include significantly richer text without burdening the PHP code with having its help loaded unnecessarily.</p>

<p>This book can be navigated hierarchically as well, making it easy to keep related topics together.</p>

<h2>Advanced Help is indexed by the search engine</h2>

<p>An important goal of this system was to add searchability to the help. By being able to enter keywords into the search box and find relevant topics, we come up with a system that resembles the kind of help that comes with many operating systems. This is very valuable when searching through manuals trying to find out how to do a particular thing.</p>

<p>This search is specific to the help, meaning that the help will not be mixed in with the global node search. This can be considered both an advantage and a disadvantage. For the most part, this help system is meant to provide help to site administrators, and content searches should not find it. The disadvantage, of course, is when you want to use it for end user help, you will not be able to.</p>

<h2>Inline help can be brought in via popups</h2>

<p>In addition to the manual-like hierarchical navigation, <strong>Advanced Help</strong> can also provide context-sensitive additional help through a popup. While popups are controversial, the argument for using them is that when getting help while on a form, <em>a popup will not throw away a user's data.</em> Browsers are not very friendly to input forms if they are not submitted, and navigating away from the form can be dangerous. There are various other solutions to this problem, but each one has a drawback. The drawbacks to popups are well known, but mostly it is the irritation of having new windows. When getting help, though, a popup is usually invited. Help should not interfere with what the operation the user is trying to complete. It differs greatly from the uninvited popup, which are usually ads or popups meant to prevent users from navigating away from a site.</p>

<p>Popups can be added to a page with plain text links or themed icon links.</p>
